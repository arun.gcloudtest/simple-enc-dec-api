from flask import render_template, jsonify, request, make_response, Blueprint
from cryptography.fernet import Fernet
from os import path

REQUEST_API = Blueprint('request_api', __name__)


def get_blueprint():
    """Return the blueprint for the main app module"""
    return REQUEST_API


def generate_fernet_key(fernet_key):
    """
    Generates a Fernet key and saves it into a file
    """
    key = Fernet.generate_key()
    if not path.exists(fernet_key):
        with open(fernet_key, "wb") as key_file:
            key_file.write(key)


def load_fernet_key():
    """
    Loads the key to memory.
    """
    fernet_key = 'fernet.key'
    generate_fernet_key(fernet_key)
    return open(fernet_key, "rb").read()


@REQUEST_API.route('/', methods=['GET'])
def main_page():
    """
    This will serve the default page.
    :return:
    """
    return render_template('index.html')


@REQUEST_API.route('/api/health', methods=['GET'])
def health():
    """
    This will serve the health api.
    :return:
    """
    return make_response(jsonify({'state': 'Healthy'}), 200)


@REQUEST_API.route('/api/encrypt', methods=['POST', 'GET'])
def encrypt():
    """
    This will encrypt the given API.
    :return: Status Code, Payload
    """
    _failed_payload = payload_resp = {"Input": None, "Output": None, "Status": "Failed"}
    try:
        if request.method == 'GET':
            _failed_payload["Message"] = 'GET Method not supported'
            return make_response(jsonify(_failed_payload), 400)
        key = load_fernet_key()
        api_payload = request.json
        if api_payload.get('Input') and len(api_payload.get('Input')):
            encoded_message = api_payload.get('Input').encode()
            fernet_enc_file = Fernet(key)
            encrypted_message = fernet_enc_file.encrypt(encoded_message)
            payload_resp = {"input": api_payload.get('Input'), "Output": encrypted_message.decode('utf-8'),
                            "Status": "success", "Message": "Encryption success"}
            return make_response(jsonify(payload_resp), 200)
        else:
            _failed_payload["Message"] = "Empty Input"
            return make_response(jsonify(_failed_payload), 200)
    except Exception as _err:
        _failed_payload["Message"] = str(_err)
        return make_response(jsonify(_failed_payload), 400)


@REQUEST_API.route('/api/decrypt', methods=['POST', 'GET'])
def decrypt():
    """
    This func will encrypt the given API
    :return: Status Code, Payload
    """
    _failed_payload = payload_resp = {"Input": None, "Output": None, "Status": "Failed"}
    try:
        if request.method == 'GET':
            _failed_payload["Message"] = 'GET Method not supported'
            return make_response(jsonify(_failed_payload), 400)
        key = load_fernet_key()
        api_payload = request.json
        if api_payload.get('Input') and len(api_payload.get('Input')):
            fernet_enc_file = Fernet(key)
            decrypted_message = fernet_enc_file.decrypt(api_payload.get('Input').encode('utf-8'))
            payload_resp = {"input": api_payload.get('Input'), "Output": decrypted_message.decode('utf-8'),
                            "Status": "success", "Message": "Decrypt success"}
            return make_response(jsonify(payload_resp), 200)
        else:
            _failed_payload["Message"] = "Empty Input"
            return make_response(jsonify(_failed_payload), 200)
    except Exception as _err:
        _failed_payload["Message"] = str(_err)
        return make_response(jsonify(_failed_payload), 400)
