from flask import Flask, render_template, jsonify, request, make_response
from flask_swagger_ui import get_swaggerui_blueprint
from routes import request_api

APP = Flask(__name__)

SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Simple Encryption and Decryption API"
    }
)
APP.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
APP.register_blueprint(request_api.get_blueprint())


@APP.errorhandler(404)
def handle_404_error(_error):
    _err = {
        "Input": request.json,
        "Output": "",
        "Status": 404,
        "Message": str(_error)
    }
    return make_response(jsonify(_err), 404)


@APP.errorhandler(400)
def handle_400_error(_error):
    """Return a http 400 error to client"""
    _err = {
        "Input": request.json,
        "Output": "",
        "Status": 400,
        "Message": str(_error)
    }
    return make_response(jsonify(_err), 400)


@APP.errorhandler(401)
def handle_401_error(_error):
    """Return a http 401 error to client"""
    _err = {
        "Input": request.json,
        "Output": "",
        "Status": 401,
        "Message": str(_error)
    }
    return make_response(jsonify(_err), 401)


@APP.errorhandler(500)
def handle_500_error(_error):
    """Return a http 500 error to client"""
    _err = {
        "Input": request.json,
        "Output": "",
        "Status": 500,
        "Message": str(_error)
    }
    return make_response(jsonify(_err), 500)


if __name__ == '__main__':
    APP.run(host='0.0.0.0', port=5000, debug=False)
