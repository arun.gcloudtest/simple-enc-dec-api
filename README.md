# Simple Encryption API
Runs on !
- Python3
- Flask
- Swagger
- Docker
- K8s

Installing Docker and Docker-Compose
```sh
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
rm -f get-docker.sh

sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

Run the API using Docker:
```sh
cd /path/to/code/directory
docker-compose up -d
```

Running the API using Kubernetes:
```sh
cd /path/to/code/directory
kubectl apply -f crypto-api.yml
```

### Encrypt Data:
```sh
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"Input":"string"}' \
  http://localhost/api/encrypt
```

### Decrypt Data:
```sh
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"Input":"Encrypted-String"}' \
  http://localhost/api/decrypt
```

### Get Health status of the API:
```sh
curl --request GET http://localhost/api/health
```

### Access Swagger UI:
http://localhost/swagger